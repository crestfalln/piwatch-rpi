import psycopg
from os import environ
from time import time



class database:
    _curs: psycopg.Cursor
    connection: psycopg.Connection
    autocommit: bool 
    add_record_recs = """INSERT INTO "Recs"
                       VALUES(%s, %s)"""
    add_record_err = """INSERT INTO "Errors"
                    VALUES(%s, %s, %s, %s)"""

    def __init__(self, autocommit = True):
        self.autocommit = autocommit
        self.connection = psycopg.connect(f"host={environ['DBHOST']} user={environ['DBUSER']} password={environ['DBPASS']} port={environ['DBPORT']} dbname={environ['DBDATA']}",
                autocommit=self.autocommit)
        self._curs = self.connection.cursor()

    def con(self) -> psycopg.Connection:
        return self.connection

    def cursor(self) -> psycopg.Cursor:
        return self._curs

    def commit(self) -> None:
        self.connection.commit()

    def execute(self, operation, params=None):
        tmp = self.cursor().execute(operation, params)
        return tmp;

    def insertIntoRecs(self, start: float , end: float):
        self.execute(self.add_record_recs, (round(start),round(end)));

    def insertIntoError(self, qualifier:str, description= "", time: float= time()):
        self.execute(self.add_record_err, (qualifier, description, 0, round(time)))

    def __del__(self):
        if('connection' in self.__dict__): 
            self.commit()
            self.connection.close()
