from serialsensor import GenericSerialSensor
from serial import Serial
from multiprocessing import Process
from multiprocessing.connection import Connection
from os import environ

def readline(conn: Serial):
    bytes = bytearray()
    while(True): 
        byte = conn.read(1)
        if(byte == b'\r'):
            break
        if(byte == b'\n'):
            break
        bytes += byte
    res = bytes.decode('utf-8')
    return res.strip('\n\r')

# Distance based porximity sensor that reads from serial 
class arduinoSensor(GenericSerialSensor):
    threshDist: float
    def __init__(self, threshDist: float = 0, serialPort: str = "", baud: int = 0, start: bool = True):
        if(threshDist == 0):
            threshDist = int(environ['THRESHOLD'])
        if(serialPort == ""):
            serialPort = environ['ACMSERIAL']
        if(baud == 0):
            baud = int(environ['BAUDRATE'])

        self.threshDist = threshDist
        super().__init__(serialPort, baud, False)
        self.serialConn.write(b"ready\n")
        self.serialConn.flush()
        first_words = self.serialConn.readline().decode('utf-8').rstrip()
        if(first_words != "ready"):
            print(first_words)
            raise ConnectionError 
        if start :
            self.start()

    def start(self):
        monitor = Process(target=self.__monitor, args= (self._childConn,))
        monitor.start()
        self.processPool.append(monitor)

    def __monitor(self, conn: Connection):
        try:
            while True:
                if (conn.poll()):
                    if(conn.recv()):
                        break
                # dist = float(readline(self.serialConn))
                dist = float(self.serialConn.readline().decode('ascii').rstrip())
                if(dist < self.threshDist):
                    self.active.value = True
                else:
                    self.active.value = False
        except ValueError:
            pass 
        except KeyboardInterrupt:
            return
