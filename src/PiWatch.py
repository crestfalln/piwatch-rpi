#!/bin/python3

import time
from errors import Timeout
from dotenv import load_dotenv
from db import database
from ultrasonic_sensor import ultrasonicSensor
from arduino import arduinoSensor
from sensor import ProxSensor

load_dotenv()


# Global Variables
TRIG = 17
ECHO = 4
state = 0
writeDir = "/var/www/html/"
waitTime = 4
trigDist = 30
checkIter = 3

try:
    db = database()
except ConnectionError as err:
    print(f"\x1b[31;1m{err}\x1b[0m")
except KeyError as err:
    print(f"\x1b[31;1m{err}\x1b[0m")
except:
    print(f"\x1b[31;1mCannot create database connection\x1b[0m")



def trigAlert(dist):
    if(dist <= trigDist):
        return True
    else:
        return False


def writeTimeFile(start:float, end:float):
    print(start, round)
    db.insertIntoRecs(start=start, end=end);

def writeErrorFile(qualifier:str, description= "", time: float= time.time()):
    db.insertIntoError(qualifier=qualifier, description=description, 
        time=time)

        

# print(time.time());
# now = time.time();
# now = now - (now % (3600*24)) + (3600*5.5)
# for i in range(-5000,5000):
#     if i == 0:
#         continue;
#     tmp = now + 3600*24*i
#     for e in range(0,23):
#         tmp = tmp + 3600*e
#         tmp = tmp + randint(0, 1800)
#         writeTimeFile(tmp , tmp + randint(5, 1700))




# Main logic
def main():
    #us = ultrasonicSensor(trig= TRIG, echo= ECHO, threshDist= trigDist)
    fake = arduinoSensor()
    sensor = ProxSensor(fake)
    try:
        while(True):
            activeTime = sensor.activity.get()
            print(activeTime.start)
    except KeyboardInterrupt:
        quit() 

if __name__ == "__main__":
    try:
        main()
    except Timeout as tout:
        writeErrorFile(qualifier=tout.qualifier, description=tout.description)
    # main()
