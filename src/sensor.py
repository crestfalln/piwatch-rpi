from sensor_interface import AbstarctProxSensor
from multiprocessing import Pipe, Process, Queue
from multiprocessing.connection import Connection
from time import sleep, time

# A wrapper over a AbstarctProxSensor
class ProxSensor:
    class ActiveTime:
        start: float
        end: float
        def __init__(self , start, end): self.start, self.end = start, end
    

    activity: Queue = Queue()
    underlyingSensor: AbstarctProxSensor
    __mainPipe: Connection
    __childPipe: Connection
    processPool: list[Process] = list[Process]()

    def __init__(self, sensorInterface: AbstarctProxSensor = AbstarctProxSensor(), start= True):
        self.underlyingSensor =  sensorInterface
        self.__mainPipe, self.__childPipe = Pipe()
        if start :
            self.start()

    def __del__(self):
        self.__mainPipe.send(True)
        for i in self.processPool:
            i.join()

    def start(self):
        monitor = Process(target=self.__monitor, args=( self.__childPipe, )) 
        monitor.start()
        self.processPool.append(monitor)
        
    def isActive(self) -> bool :
        return self.underlyingSensor.isActive()

    def __monitor(self, conn: Connection):
        try:
            while True:
                if(conn.poll()):
                    if(conn.recv()):
                        break
                if(self.isActive()):
                    start = time()
                    while(self.isActive()):
                        sleep(0.0001)
                        pass             
                    end = time()
                    if(end - start > 0.5):
                        self.activity.put(self.ActiveTime(start, end))
                sleep(0.0001)             
        except KeyboardInterrupt:
            return
