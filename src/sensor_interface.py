from time import sleep

# Abstarct prox sensor interface inherited by all prox sensors
class AbstarctProxSensor: 

    def when_activated(self):
        pass
    def when_deactivated(self):
        pass
    def isActive(self) -> bool:
        sleep(0.1)
        return False
     
