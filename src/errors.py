class PiWatchException(BaseException):
    qualifier: str
    description: str
    def __init__(self, qualifier:str, description:str = ""):
        self.qualifier = qualifier
        self.description = description

class Timeout(PiWatchException):
    def __init__(self, qualifier:str, description:str = ""):
        PiWatchException.__init__(self, qualifier, description)
