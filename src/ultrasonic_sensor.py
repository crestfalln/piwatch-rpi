from ctypes import c_bool
from multiprocessing import Value
from time import sleep
from typing import Any
from gpiozero import DistanceSensor
from sensor_interface import AbstarctProxSensor
from os import environ

# Distance based sensor that reads from GPIO 
class ultrasonicSensor(AbstarctProxSensor):
    underlyingSensor: DistanceSensor
    active: Any

    def __on_activation(self):
       self.active.value = True         
       self.when_activated()

    def __on_deactivation(self):
       self.active.value = False         
       self.when_deactivated()

    def isActive(self):
        return self.active.value

    def distance(self):
        toRet = self.underlyingSensor.distance*100
        sleep(0.2)
        return toRet
    
    def __init__(self, echo=0, trig=0, threshDist=0 ):
        if(threshDist == 0):
            threshDist = int(environ['THRESHOLD'])
        if(echo == 0):
            echo = int(environ['ECHO'])
        if(trig == 0):
            trig = int(environ['TRIG'])
        self.underlyingSensor = DistanceSensor(echo= echo, trigger= trig, 
            max_distance=4, threshold_distance=threshDist/(100*4))
            
        self.underlyingSensor.when_activated = self.__on_activation
        self.underlyingSensor.when_deactivated = self.__on_deactivation
        self.active = Value(c_bool)
        super().__init__()


