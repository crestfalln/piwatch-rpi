from time import sleep
from sensor_interface import AbstarctProxSensor
from ctypes import c_bool
from multiprocessing import Value, Process, Pipe
from multiprocessing.connection import Connection
from typing import Any
import serial

# Generic sensor that reads state from a serial connection
class GenericSerialSensor(AbstarctProxSensor):
    active: Any
    processPool: list[Process] = list()
    serialConn: serial.Serial
    __mainConn: Connection
    _childConn: Connection

    def __init__(self, serialPort: str , baud:int = 9600, start:bool = True):
        self.active = Value(c_bool)
        self.active.value = False
        self.serialConn = serial.Serial(port=serialPort, baudrate= baud)
        sleep(2)
        self.__mainConn, self._childConn = Pipe()
        if start:
            self.start()

    def __del__(self):
        self.__mainConn.send(True)
        for i in self.processPool:
            i.join()
            i.join()

    def start(self):
        monitor = Process(target=self.__monitor, args= (self._childConn,))
        monitor.start()
        self.processPool.append(monitor)

    def isActive(self):
        return self.active.value

    def __monitor(self, conn: Connection):
        try:
            while True:
                if (conn.poll()):
                    if(conn.recv()):
                        break
                val = self.serialConn.read(1)
                self.serialConn.write(str(val).encode('utf-8'))
                if(val == b'y'):
                    self.active.value = True
                if(val == b'n'):
                    self.active.value = False
        except KeyboardInterrupt:
            return
